package net.plussycraft.regionmarket.worldutils;


import org.bukkit.Material;
import org.bukkit.block.Block;

import java.io.Serializable;

/**
 * Created by Gust09 on 27/06/2016.
 */
public class SerializableBlock implements Serializable {

    private Material material;
    private byte data;

    public SerializableBlock(Block b){
        this.material = b.getType();
        this.data = b.getData();
    }


    public Material getMaterial() {
        return material;
    }

    public byte getData() {
        return data;
    }

}
