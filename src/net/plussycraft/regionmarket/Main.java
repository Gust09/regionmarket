package net.plussycraft.regionmarket;

import net.milkbowl.vault.economy.Economy;
import net.plussycraft.regionmarket.commands.AdminCmd;
import net.plussycraft.regionmarket.commands.RegionMarketCmd;
import net.plussycraft.regionmarket.gui.ConfirmUnrentMenu;
import net.plussycraft.regionmarket.gui.ListHubMenu;
import net.plussycraft.regionmarket.gui.MainMenu;
import net.plussycraft.regionmarket.listeners.*;
import net.plussycraft.regionmarket.managers.CellManager;
import net.plussycraft.regionmarket.objects.Cell;
import net.plussycraft.regionmarket.storage.CellFiles;
import net.plussycraft.regionmarket.storage.Config;
import net.plussycraft.regionmarket.utils.Log;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Server;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gust09 on 27/06/2016.
 */
public class Main extends JavaPlugin {

    private static Server server;
    private static Main plugin;

    private static List<Cell> cells;
    private static List<Chunk> chunks;
    public static Economy econ = null;

    public void onEnable(){
        server = Bukkit.getServer();
        plugin = this;
        cells = new ArrayList<>();
        chunks = new ArrayList<>();

        Config.setup();

        // Register Stuff
        server.getPluginManager().registerEvents(new SignListener(), plugin);
        server.getPluginManager().registerEvents(new ListMenuListener(), plugin);
        server.getPluginManager().registerEvents(new ChunkListener(), plugin);
        server.getPluginManager().registerEvents(new MainMenu(), plugin);
        server.getPluginManager().registerEvents(new ListHubMenu(), plugin);
        server.getPluginManager().registerEvents(new CPMenuListener(), plugin);
        server.getPluginManager().registerEvents(new CellTakenMenuListener(), plugin);
        server.getPluginManager().registerEvents(new EmptyCellMenuListener(), plugin);
        server.getPluginManager().registerEvents(new ConfirmUnrentMenu(), plugin);

        getCommand("regionmarket").setExecutor(new RegionMarketCmd());
        getCommand("regionmarketadmin").setExecutor(new AdminCmd());

        loadCells();

        CellManager.runCheckTimer();

        if (!setupEconomy() ) {
            Log.warning("Disabled because no economy plugin was found");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        CellManager.updatePlayersRenting();

    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    public static Main get() {
        return plugin;
    }


    public static void loadCells(){
        File cells = new File(plugin.getDataFolder() + "/cells");
        File schematics = new File(plugin.getDataFolder() + "/schematics");

        if(!cells.exists()){
            try {
                cells.mkdirs();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if(!schematics.exists()){
            try {
                schematics.mkdirs();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        for(File file : cells.listFiles()){
            if(!file.getName().endsWith(".cell"))
                continue;
            CellFiles.load(file.getName().replace(".cell", ""));
        }

        for (Cell c : Main.cells) {
            Chunk chunk = c.getSign().getChunk();
            if (!getChunks().contains(chunk)) {
                addChunk(chunk);
            }
        }
    }

    public static List<Cell> getCells() {
        return cells;
    }

    public static List<Chunk> getChunks() {
        return chunks;
    }

    public static void addChunk(Chunk chunk) {
        chunks.add(chunk);
    }

    public static void removeChunk(Chunk chunk) {
        chunks.add(chunk);
    }

    public static Cell getCellByName(String name) {
        Cell cell = null;
        for(Cell c : cells){
            if (c.getName().equalsIgnoreCase(name)) {
                cell = c;
                break;
            }
        }
        return cell;
    }

    public static Cell getCellByPlayer(String player) {
        Cell cell = null;

        for (Cell c : cells) {
            if (c.getOwner().equalsIgnoreCase(player)) {
                cell = c;
                break;
            }
        }
        return cell;
    }

    public static void addCell(Cell cell){
        cells.add(cell);
    }

    public static void removeCell(Cell cell){
        cells.remove(cell);
    }

}
