package net.plussycraft.regionmarket.objects;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import net.plussycraft.regionmarket.storage.Config;
import net.plussycraft.regionmarket.utils.ChatUtils;
import net.plussycraft.regionmarket.utils.Utils;
import net.plussycraft.regionmarket.utils.WGUtils;
import org.bukkit.World;
import org.bukkit.block.Sign;

import java.io.Serializable;

/**
 * Created by Gust09 on 30/06/2016.
 */
public class Cell implements Serializable {

    private transient ProtectedRegion region;
    private transient Sign sign;
    private String name;
    private String signLocation;
    private String owner;
    private double price;
    private long lastpaid;
    private long paydelay;
    private int x;
    private int y;
    private int z;
    private int area;

    public Cell(ProtectedRegion region, Sign sign){
        this.region = region;
        this.name = region.getId();
        this.sign = sign;
        this.owner = "";
        this.lastpaid = 0;
        this.paydelay = Config.getRenewDelay();
        this.signLocation = Utils.getStringFromLocation(sign.getLocation());

        int[] dimensions = WGUtils.getDimentions(region);

        this.x = dimensions[0];
        this.y = dimensions[1];
        this.z = dimensions[2];

        this.area = x*y*z;
        this.price = 6*area;

        updateSign();
    }

    public void load(){
        this.sign = (Sign) Utils.getLocationFromString(signLocation).getBlock().getState();
        this.region = WGUtils.getRegion(sign.getWorld(), name);
        updateSign();
    }

    public void updateSign(){
        sign.setLine(0, ChatUtils.colorize("[Cela]"));
        sign.setLine(1, ChatUtils.colorize("&f&m---------------"));

        if(owner.isEmpty()){
            sign.setLine(2, ChatUtils.colorize("&aArrendar"));
            sign.setLine(3, ChatUtils.colorize((int)price+"€/Dia"));
        }
        else{
            sign.setLine(2, ChatUtils.colorize("&4Dono:"));
            sign.setLine(3, ChatUtils.colorize(owner));
        }
        sign.update();
    }

    public ProtectedRegion getRegion() {
        return region;
    }

    public void setRegion(ProtectedRegion region) {
        this.region = region;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public long getLastpaid(){
        return lastpaid;
    }

    public void setLastpaid(long lastpaid) {
        this.lastpaid = lastpaid;
    }

    public long getPaydelay() {
        return paydelay;
    }

    public void setPaydelay(long paydelay) {
        this.paydelay = paydelay;
    }

    public String getName() {
        return name;
    }

    public World getWorld(){
        return sign.getWorld();
    }

    public int getX(){
        return x;
    }

    public int getY(){
        return y;
    }

    public int getZ(){
        return z;
    }

    public int getArea(){
        return area;
    }

    public Sign getSign(){
        return sign;
    }

    public String getDimentions(){
        return x+"x"+z+"x"+y;
    }

}
