package net.plussycraft.regionmarket.gui;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import net.plussycraft.regionmarket.Main;
import net.plussycraft.regionmarket.objects.Cell;
import net.plussycraft.regionmarket.utils.ChatUtils;
import net.plussycraft.regionmarket.utils.ItemAPI;
import net.plussycraft.regionmarket.utils.WGUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Gust09 on 05/07/2016.
 */
public class ListHubMenu implements Listener {

    private static int SIZE = 1 * 9;
    private static Inventory MENU = Bukkit.createInventory(null, SIZE, ChatUtils.colorize("&6&lLISTA DAS CELAS"));

    private static ItemStack SHOW_FREE = ItemAPI.create(Material.WOOL, (byte) 13, 1, "&2&lCelas Livres");
    private static ItemStack SHOW_TAKEN = ItemAPI.create(Material.WOOL, (byte) 14, 1, "&4&lCelas Ocupadas");
    private static ItemStack YOUR_CELL = ItemAPI.create(Material.BED, 1, "&a&lA tua Cela");
    private static ItemStack GO_BACK = ItemAPI.create(Material.STAINED_CLAY, (byte) 14, 1, "&c&lVoltar");

    public static void setup(Player p) {

        ProtectedRegion region = WGUtils.getRegion(p);
        List<ProtectedRegion> list = WGUtils.getIntersectingRegions(region, p.getWorld());

        int free = 0;
        int taken = 0;

        for (ProtectedRegion r : list) {
            if (r.hasMembersOrOwners()) {
                taken++;
            } else {
                free++;
            }
        }

        SHOW_FREE = ItemAPI.create(Material.WOOL, (byte) 13, 1, "&2&lCelas Livres", Arrays.asList("&eExistem &6" + free + " &ecelas livres neste bloco", "&eClica para as veres"));
        SHOW_TAKEN = ItemAPI.create(Material.WOOL, (byte) 14, 1, "&4&lCelas Ocupadas", Arrays.asList("&eExistem &6" + taken + " &ecelas ocupadas neste bloco", "&eClica para as veres"));
        YOUR_CELL = ItemAPI.create(Material.BED, 1, "&a&lA tua Cela", Arrays.asList("&eClica para ver as opções da tua cela"));
        GO_BACK = ItemAPI.create(Material.STAINED_CLAY, (byte) 14, 1, "&c&lVoltar", Arrays.asList("&eVoltar ao Menu anterior"));

        MENU.setItem(0, SHOW_FREE);
        MENU.setItem(1, SHOW_TAKEN);
        MENU.setItem(2, YOUR_CELL);
        MENU.setItem(8, GO_BACK);

        p.openInventory(MENU);

    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();

        if (e.getClickedInventory() == null)
            return;

        if (e.getClickedInventory().equals(MENU)) {
            e.setCancelled(true);

            ItemStack item = e.getCurrentItem();

            if (item.equals(SHOW_FREE)) {
                new ListMenu(WGUtils.getRegion(p), "FREE").show(p, 1);
            } else if (item.equals(SHOW_TAKEN)) {
                new ListMenu(WGUtils.getRegion(p), "TAKEN").show(p, 1);
            } else if (item.equals(YOUR_CELL)) {

                Cell cell = Main.getCellByPlayer(p.getName());

                if (cell == null) {
                    ChatUtils.msg(p, "&6Prison &8> &4Tu não tens nenhuma cela arrendada!");
                    p.closeInventory();
                    return;
                }

                new CPMenu().show(p);
            } else if (item.equals(GO_BACK)) {
                MainMenu.setup(p);
            }
        }
    }


}
