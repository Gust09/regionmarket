package net.plussycraft.regionmarket.gui;

import net.plussycraft.regionmarket.Main;
import net.plussycraft.regionmarket.objects.Cell;
import net.plussycraft.regionmarket.utils.ChatUtils;
import net.plussycraft.regionmarket.utils.ItemAPI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by Gust09 on 05/07/2016.
 */
public class CellTakenMenu {

    public static HashMap<String, CellTakenMenu> MENUS = new HashMap<>();

    private int SIZE = 1 * 9;
    private Inventory MENU = Bukkit.createInventory(null, SIZE, ChatUtils.colorize("&6&lCELA OCUPADA"));

    private ItemStack OWNER;
    private ItemStack COORDS;
    private ItemStack TELEPORT;
    private ItemStack CLOSE;

    private ItemStack selected;

    public CellTakenMenu() {
    }

    public void show(Player p, ItemStack item) {

        selected = item;

        Cell cell = Main.getCellByName(ChatUtils.stripColor(item.getItemMeta().getDisplayName()).split(" ")[1]);
        if (cell == null) {
            return;
        }

        int x = cell.getSign().getLocation().getBlockX();
        int y = cell.getSign().getLocation().getBlockY() - 2;
        int z = cell.getSign().getLocation().getBlockZ();

        OWNER = ItemAPI.create(Material.SKULL_ITEM, (byte) 3, 1, "&a&lDono da Cela", Arrays.asList("&e" + cell.getOwner()));
        COORDS = ItemAPI.create(Material.COMPASS, 1, "&a&lCoordenadas", Arrays.asList("&eX: &6" + x + " &f/ &eY: &6" + y + " &f/ &eZ: &6" + z));
        TELEPORT = ItemAPI.create(Material.ENDER_PEARL, 1, "&a&lTeleportar para a Cela", Arrays.asList("&eClica para te teleportares para esta cela"));
        CLOSE = ItemAPI.create(Material.STAINED_CLAY, (byte) 14, 1, "&c&lFechar", Arrays.asList("&eFechar o Menu"));

        MENU.clear();

        MENU.setItem(0, OWNER);
        MENU.setItem(1, COORDS);
        MENU.setItem(2, TELEPORT);
        MENU.setItem(8, CLOSE);

        MENUS.put(p.getName(), this);

        p.openInventory(MENU);
    }

    public ItemStack getSelected() {
        return selected;
    }

}
