package net.plussycraft.regionmarket.gui;

import net.plussycraft.regionmarket.utils.ChatUtils;
import net.plussycraft.regionmarket.utils.ItemAPI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

/**
 * Created by Gust09 on 05/07/2016.
 */
public class MainMenu implements Listener {

    private static int SIZE = 1 * 9;
    private static Inventory MENU = Bukkit.createInventory(null, SIZE, ChatUtils.colorize("&6&lAGENTE IMOBILIARIO"));

    private static ItemStack TUTORIAL = ItemAPI.create(Material.EMPTY_MAP, 1, "&a&lTutorial das Celas", Arrays.asList("&eClica para saberes como ter uma cela"));
    private static ItemStack LIST = ItemAPI.create(Material.EMPTY_MAP, 1, "&a&lLista das Celas", Arrays.asList("&eClica para ver a lista da celas"));
    private static ItemStack CLOSE = ItemAPI.create(Material.STAINED_CLAY, (byte) 14, 1, "&c&lFechar", Arrays.asList("&eFechar o Menu"));

    public static void setup(Player p) {

        MENU.setItem(0, TUTORIAL);
        MENU.setItem(1, LIST);
        MENU.setItem(8, CLOSE);

        p.openInventory(MENU);

    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();

        if (e.getClickedInventory() == null)
            return;

        if (e.getClickedInventory().equals(MENU)) {
            e.setCancelled(true);

            ItemStack item = e.getCurrentItem();

            if (item.equals(TUTORIAL)) {
                p.closeInventory();
                p.chat("/tutorial-celas");
            } else if (item.equals(LIST)) {
                ListHubMenu.setup(p);
            } else if (item.equals(CLOSE)) {
                p.closeInventory();
            }
        }
    }


}
