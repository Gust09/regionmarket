package net.plussycraft.regionmarket.gui;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import net.plussycraft.regionmarket.Main;
import net.plussycraft.regionmarket.objects.Cell;
import net.plussycraft.regionmarket.utils.ChatUtils;
import net.plussycraft.regionmarket.utils.ItemAPI;
import net.plussycraft.regionmarket.utils.WGUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Gust09 on 05/07/2016.
 */
public class ListMenu {

    public static HashMap<String, ListMenu> MENUS = new HashMap<>();

    public static ItemStack PAGE_NEXT = ItemAPI.create(Material.SIGN, 1, "&e&lPágina Seguinte &6>>>");
    public static ItemStack PAGE_BEFORE = ItemAPI.create(Material.SIGN, 1, "&6<<< &e&lPágina Anterior");
    public static ItemStack GO_BACK = ItemAPI.create(Material.STAINED_CLAY, (byte) 14, 1, "&c&lVoltar", Arrays.asList("&eVoltar ao menu anterior"));
    public static HashMap<String, Integer> PAGES = new HashMap<>();

    private int SIZE = 6*9;
    private int PAGE_SIZE = SIZE-9;
    private Inventory MENU = Bukkit.createInventory(null, SIZE, ChatUtils.colorize("&6&lLISTA DAS CELAS"));
    private ProtectedRegion REGION;
    private String FILTER;

    private ItemStack selected;

    public ListMenu(ProtectedRegion region, String filter) {
        this.REGION = region;
        this.FILTER = filter;
    }

    public void show(Player p, int page){

        List<ProtectedRegion> list = WGUtils.getIntersectingRegions(REGION, p.getWorld());
        List<Cell> filtered = new ArrayList<>();

        MENUS.put(p.getName(), this);

        if (FILTER.equals("FREE")) {
            for (ProtectedRegion r : list) {
                if (Main.getCellByName(r.getId()) == null)
                    continue;
                if (!r.hasMembersOrOwners())
                    filtered.add(Main.getCellByName(r.getId()));
            }
        } else if (FILTER.equals("TAKEN")) {
            for (ProtectedRegion r : list) {
                if (Main.getCellByName(r.getId()) == null)
                    continue;
                if (r.hasMembersOrOwners())
                    filtered.add(Main.getCellByName(r.getId()));
            }
        }

        PAGES.put(p.getName(), page);
        int pageFirst = (page - 1) * PAGE_SIZE;
        int pageLast = pageFirst + PAGE_SIZE;
        int current = 0;

        MENU.clear();

        for (Cell c : filtered) {
            if(current >= pageFirst && current < pageLast){
                String owner = "Sem dono";

                if (!c.getOwner().isEmpty()) {
                    owner = c.getOwner();
                }

                MENU.addItem(ItemAPI.create(Material.IRON_FENCE, 1, "&a&lCELA " + c.getName().toUpperCase(),
                        Arrays.asList(
                                "&eDono: &6" + owner,
                                "&eDimensões: &6" + c.getDimentions(),
                                "&eRenda: &6" + (int) c.getPrice() + "€/Dia"
                        )));


            }
            current++;
        }


        if (page != 1) {
            MENU.setItem(PAGE_SIZE, PAGE_BEFORE);
        }
        if (!(pageFirst + PAGE_SIZE + 1 > filtered.size())) {
            MENU.setItem(PAGE_SIZE + 8, PAGE_NEXT);
        }

        MENU.setItem(PAGE_SIZE + 3, ItemAPI.create(Material.PAPER, page, "&ePágina &6" + page));
        MENU.setItem(PAGE_SIZE + 5, GO_BACK);
        p.openInventory(MENU);
    }


    public ItemStack getSelected() {
        return selected;
    }
}
