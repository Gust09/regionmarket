package net.plussycraft.regionmarket.gui;

import net.plussycraft.regionmarket.utils.ChatUtils;
import net.plussycraft.regionmarket.utils.ItemAPI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

/**
 * Created by Gust09 on 05/07/2016.
 */
public class ConfirmUnrentMenu implements Listener {

    private static int SIZE = 1 * 9;
    private static Inventory MENU = Bukkit.createInventory(null, SIZE, ChatUtils.colorize("&6&lABANDONAR CELA"));

    private static ItemStack CONFIRM = ItemAPI.create(Material.STAINED_CLAY, (byte) 13, 1, "&a&lConfirmar", Arrays.asList("&eClica para abandonares a tua cela", "&c&lAVISO: Vais perder todos os itens que tens na cela!"));
    private static ItemStack CANCEL = ItemAPI.create(Material.STAINED_CLAY, (byte) 14, 1, "&c&lCancelar", Arrays.asList("&eClica para voltares ao menu anterior"));

    public static void setup(Player p) {

        MENU.setItem(0, CONFIRM);
        MENU.setItem(8, CANCEL);

        p.openInventory(MENU);

    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();

        if (e.getClickedInventory() == null)
            return;

        if (e.getClickedInventory().equals(MENU)) {
            e.setCancelled(true);

            ItemStack item = e.getCurrentItem();

            if (item.equals(CONFIRM)) {
                p.chat("/anular-renda");
                p.closeInventory();
            } else if (item.equals(CANCEL)) {
                CPMenu menu = CPMenu.MENUS.get(p.getName());
                if (menu != null) {
                    menu.show(p);
                } else {
                    menu = new CPMenu();
                    menu.show(p);
                }
            }
        }
    }


}
