package net.plussycraft.regionmarket.gui;

import net.plussycraft.regionmarket.Main;
import net.plussycraft.regionmarket.objects.Cell;
import net.plussycraft.regionmarket.utils.ChatUtils;
import net.plussycraft.regionmarket.utils.ItemAPI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by Gust09 on 05/07/2016.
 */
public class CPMenu implements Listener {

    public static HashMap<String, CPMenu> MENUS = new HashMap<>();

    private int SIZE = 1 * 9;
    private Inventory MENU = Bukkit.createInventory(null, SIZE, ChatUtils.colorize("&6&lA TUA CELA"));

    private ItemStack selected;

    private ItemStack OWNER;
    private ItemStack COORDS;
    private ItemStack TELEPORT;
    private ItemStack NEXT_RENT;
    private ItemStack LEAVE;
    private ItemStack CLOSE;

    public CPMenu() {
    }

    public void show(Player p) {

        Cell cell = Main.getCellByPlayer(p.getName());
        if (cell == null) {
            ChatUtils.msg(p, "&6Prison &8> &4Tu não tens nenhuma cela arrendada!");
            return;
        }

        int x = cell.getSign().getLocation().getBlockX();
        int y = cell.getSign().getLocation().getBlockY() - 2;
        int z = cell.getSign().getLocation().getBlockZ();

        OWNER = ItemAPI.create(Material.SKULL_ITEM, (byte) 3, 1, "&a&lDono da Cela", Arrays.asList("&e" + cell.getOwner()));
        COORDS = ItemAPI.create(Material.COMPASS, 1, "&a&lCoordenadas", Arrays.asList("&eX: &6" + x + " &f/ &eY: &6" + y + " &f/ &eZ: &6" + z));
        TELEPORT = ItemAPI.create(Material.ENDER_PEARL, 1, "&a&lTeleportar para a Cela", Arrays.asList("&eClica para te teleportares para a tua cela"));
        NEXT_RENT = ItemAPI.create(Material.WATCH, 1, "&a&lPróxima Renda", Arrays.asList("&e" + ChatUtils.convertMillis(cell.getLastpaid() + cell.getPaydelay() - System.currentTimeMillis())));
        LEAVE = ItemAPI.create(Material.BARRIER, 1, "&a&lAbandonar a Cela", Arrays.asList("&eClica para abandonares a tua cela"));
        CLOSE = ItemAPI.create(Material.STAINED_CLAY, (byte) 14, 1, "&c&lFechar", Arrays.asList("&eFechar o Menu"));

        MENU.clear();

        MENU.setItem(0, OWNER);
        MENU.setItem(1, COORDS);
        MENU.setItem(2, TELEPORT);
        MENU.setItem(3, NEXT_RENT);
        MENU.setItem(4, LEAVE);
        MENU.setItem(8, CLOSE);

        MENUS.put(p.getName(), this);

        p.openInventory(MENU);

    }

}
