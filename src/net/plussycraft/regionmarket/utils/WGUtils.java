package net.plussycraft.regionmarket.utils;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class WGUtils {

    public static boolean isInRegion(Player p, String regionName) {
        for(ProtectedRegion r : WGBukkit.getRegionManager(p.getWorld()).getApplicableRegions(p.getLocation())) {
            if(r.getId().equalsIgnoreCase(regionName)){
                return true;
            }
        }
        return false;
    }

    public static ProtectedRegion getRegion(World world, String name){
        return WGBukkit.getRegionManager(world).getRegion(name);
    }

    public static ProtectedRegion getRegion(Player p){
        ProtectedRegion region = null;
        for(ProtectedRegion r : WGBukkit.getRegionManager(p.getWorld()).getApplicableRegions(p.getLocation())) {
            if(r.getId().equalsIgnoreCase("__global__")){
                continue;
            }
            else{
                region = r;
            }
        }
        return region;
    }

    public static ProtectedRegion getRegion(Location loc){
        ProtectedRegion region = null;
        RegionManager manager = WGBukkit.getRegionManager(loc.getWorld());
        ApplicableRegionSet set = manager.getApplicableRegions(loc);

        for(ProtectedRegion r : set) {
            if(r.getId().equalsIgnoreCase("__global__")){
                continue;
            }
            else{
                region = r;
                break;
            }
        }
        return region;
    }

    public static int[] getDimentions(ProtectedRegion region){

        int[] dimensions = new int[3];

        int minX = region.getMinimumPoint().getBlockX();
        int minY = region.getMinimumPoint().getBlockY();
        int minZ = region.getMinimumPoint().getBlockZ();

        int maxX = region.getMaximumPoint().getBlockX();
        int maxY = region.getMaximumPoint().getBlockY();
        int maxZ = region.getMaximumPoint().getBlockZ();

        dimensions[0] = (maxX - minX) + 1;
        dimensions[1] = (maxY - minY) + 1;
        dimensions[2] = (maxZ - minZ) + 1;

        return dimensions;

    }

    public static List<ProtectedRegion> getIntersectingRegions(ProtectedRegion region, World world) {
        List<ProtectedRegion> regions = new ArrayList<ProtectedRegion>();
        List<ProtectedRegion> intersecting = new ArrayList<ProtectedRegion>();
        RegionManager rm = WGBukkit.getPlugin().getGlobalRegionManager().get(world);
        for(Map.Entry<String, ProtectedRegion> entry : rm.getRegions().entrySet()){
            regions.add(entry.getValue());
        }

        for (ProtectedRegion inside : region.getIntersectingRegions(regions)) {
            if (inside.getId().equalsIgnoreCase(region.getId())) continue;
            intersecting.add(inside);
        }
        return intersecting;
    }


}
