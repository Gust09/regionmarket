package net.plussycraft.regionmarket.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * Created by Fábio Almeida on 01/12/2015.
 */
public class Utils {

    public static Location getBlockLocationFromString(String string){
        String[] info = string.split("#");
        Location loc = new Location(Bukkit.getWorld(info[0]),
                Double.parseDouble(info[1]),
                Double.parseDouble(info[2]),
                Double.parseDouble(info[3]));
        return loc;
    }

    public static String getStringFromBlockLocation(Location location){
        String str = location.getWorld().getName() + "#" +
                location.getBlockX() + "#" +
                location.getBlockY() + "#" +
                location.getBlockZ();
        return str;
    }

    public static Location getLocationFromString(String string){
        String[] info = string.split(" ");
        Location loc = new Location(Bukkit.getWorld(info[0]),
                                    Double.parseDouble(info[1]),
                                    Double.parseDouble(info[2]),
                                    Double.parseDouble(info[3]));
        return loc;
    }

    public static String getStringFromLocation(Location location){
        String str = location.getWorld().getName() + " " +
                     location.getX() + " " +
                     location.getY() + " " +
                     location.getZ() + " ";
        return str;
    }

    public static Location getLocationWithYawPitchFromString(String string){
        String[] info = string.split(" ");
        Location loc = new Location(Bukkit.getWorld(info[0]),
                Double.parseDouble(info[1]),
                Double.parseDouble(info[2]),
                Double.parseDouble(info[3]),
                Float.parseFloat(info[4]),
                Float.parseFloat(info[5]));
        return loc;
    }

    public static String getStringFromLocationWithYawPitch(Location location){
        String str = location.getWorld().getName() + " " +
                location.getX() + " " +
                location.getY() + " " +
                location.getZ() + " " +
                location.getYaw() + " " +
                location.getPitch();
        return str;
    }

    public static String formatPoints(int points) {

        String formattedPoints = "";

        if (points < 0) formattedPoints = ChatColor.DARK_RED + String.valueOf(points);
        if (points == 0) formattedPoints = ChatColor.GRAY + String.valueOf(points);
        if (points > 0) formattedPoints = ChatColor.GOLD + String.valueOf(points);

        return formattedPoints;
    }

    public static boolean tryTeleport(Player player, Location location){
        try{
            player.teleport(location);
            return true;
        } catch(NullPointerException ex){
            return false;
        }
    }

}
