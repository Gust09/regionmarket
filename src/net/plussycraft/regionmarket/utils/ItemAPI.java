package net.plussycraft.regionmarket.utils;

import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.List;

public class ItemAPI {

	public static ItemStack create(Material material) {
		ItemStack item = new ItemStack(material);
		return item;
	}

	public static ItemStack create(Material material, int ammount) {
		ItemStack item = new ItemStack(material);
		item.setAmount(ammount);
		return item;
	}

	@SuppressWarnings("deprecation")
	public static ItemStack create(Material material, byte data, int ammount) {
		ItemStack item = new ItemStack(material);
		item.setAmount(ammount);
		item.setDurability(data);
		return item;
	}

	@SuppressWarnings("deprecation")
	public static ItemStack create(Material material, short data, int ammount) {
		ItemStack item = new ItemStack(material);
		item.setAmount(ammount);
		item.setDurability(data);
		return item;
	}

	public static ItemStack create(Material material, int ammount, String name) {
		ItemStack item = new ItemStack(material);
		item.setAmount(ammount);
		ItemMeta iMeta = item.getItemMeta();
		iMeta.setDisplayName(ChatUtils.colorize(name));
		item.setItemMeta(iMeta);
		return item;
	}

	@SuppressWarnings("deprecation")
	public static ItemStack create(Material material, byte data, int ammount,
			String name) {
		ItemStack item = new ItemStack(material);
		item.setAmount(ammount);
		item.setDurability(data);
		ItemMeta iMeta = item.getItemMeta();
		iMeta.setDisplayName(ChatUtils.colorize(name));
		item.setItemMeta(iMeta);
		return item;
	}

	@SuppressWarnings("deprecation")
	public static ItemStack create(Material material, short data, int ammount,
								   String name) {
		ItemStack item = new ItemStack(material);
		item.setAmount(ammount);
		item.setDurability(data);
		ItemMeta iMeta = item.getItemMeta();
		iMeta.setDisplayName(ChatUtils.colorize(name));
		item.setItemMeta(iMeta);
		return item;
	}

	public static ItemStack create(Material material, int ammount, String name,
			List<String> lore) {
		ItemStack item = new ItemStack(material);
		item.setAmount(ammount);
		ItemMeta iMeta = item.getItemMeta();
		iMeta.setDisplayName(ChatUtils.colorize(name));
		iMeta.setLore(colorizeLore(lore));
		item.setItemMeta(iMeta);
		return item;
	}

	@SuppressWarnings("deprecation")
	public static ItemStack create(Material material, byte data, int ammount,
			String name, List<String> lore) {
		ItemStack item = new ItemStack(material);
		item.setAmount(ammount);
		item.setDurability(data);
		ItemMeta iMeta = item.getItemMeta();
		iMeta.setDisplayName(ChatUtils.colorize(name));
		iMeta.setLore(colorizeLore(lore));
		item.setItemMeta(iMeta);
		return item;
	}

	@SuppressWarnings("deprecation")
	public static ItemStack create(Material material, short data, int ammount,
								   String name, List<String> lore) {
		ItemStack item = new ItemStack(material);
		item.setAmount(ammount);
		item.setDurability(data);
		ItemMeta iMeta = item.getItemMeta();
		iMeta.setDisplayName(ChatUtils.colorize(name));
		iMeta.setLore(colorizeLore(lore));
		item.setItemMeta(iMeta);
		return item;
	}

	public static ItemStack enchant(ItemStack item, Enchantment enchant, int lvl){
		ItemMeta iMeta = item.getItemMeta();
		iMeta.addEnchant(enchant, lvl, true);
		item.setItemMeta(iMeta);
		return item;
	}

	public static ItemStack colorize(ItemStack leatherArmor, int red, int green, int blue){
		LeatherArmorMeta lMeta = (LeatherArmorMeta) leatherArmor.getItemMeta();
		lMeta.setColor(Color.fromRGB(red, green, blue));
		leatherArmor.setItemMeta(lMeta);
		return leatherArmor;
	}

	public static ItemStack colorize(ItemStack leatherArmor, Color color){
		LeatherArmorMeta lMeta = (LeatherArmorMeta) leatherArmor.getItemMeta();
		lMeta.setColor(color);
		leatherArmor.setItemMeta(lMeta);
		return leatherArmor;
	}

	public static ItemStack makeSkull(ItemStack item, String skinName){
		try{
			SkullMeta sMeta = (SkullMeta) item.getItemMeta();
			sMeta.setOwner(skinName);
			item.setItemMeta(sMeta);
		}catch(Exception ex){
			Log.warning("Erro ao conseguir uma Skin");
		}
		return item;
	}

	public static ItemStack makeSkull(String skinName){
		ItemStack item = create(Material.SKULL_ITEM);
		try{
			SkullMeta sMeta = (SkullMeta) item.getItemMeta();
			sMeta.setOwner(skinName);
			item.setItemMeta(sMeta);
		}catch(Exception ex){
			Log.warning("Erro ao conseguir uma Skin");
		}
		return item;
	}

    public static ItemStack createHead(String skinName, String itemName) {
        ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());

        SkullMeta sMeta = (SkullMeta) item.getItemMeta();
        sMeta.setOwner(skinName);
        sMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', (itemName)));
        item.setItemMeta(sMeta);

        return item;
    }

    public static ItemStack createHead(String skinName, String itemName, List<String> lore) {
        String name = Bukkit.getOfflinePlayer(skinName).getName();
        ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());

        SkullMeta sMeta = (SkullMeta) item.getItemMeta();
        sMeta.setOwner(skinName);
        sMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', (itemName)));
        sMeta.setLore(colorizeLore(lore));
        item.setItemMeta(sMeta);

        return item;
    }

    public static ItemStack createHead(String skinName) {
        ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());

        SkullMeta sMeta = (SkullMeta) item.getItemMeta();
        sMeta.setOwner(skinName);
        item.setItemMeta(sMeta);

        return item;
    }

    private static List<String> colorizeLore(List<String> list){
        List<String> l = new ArrayList<>();
        for(String s : list){
            l.add(ChatUtils.colorize(s));
        }
        return l;
    }

}
