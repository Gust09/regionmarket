package net.plussycraft.regionmarket.utils;

import org.bukkit.Bukkit;
import org.fusesource.jansi.Ansi;

import java.util.logging.Logger;

/**
 * Created by Gust09 on 9/26/2015.
 */
public class Log {

    private static Logger logger = Bukkit.getLogger();

    public static void info(String msg){
        logger.info(Ansi.ansi().fg(Ansi.Color.GREEN).bold().toString() + "[RegionMarket] " + msg + Ansi.ansi().reset());
    }

    public static void warning(String msg){
        logger.warning(Ansi.ansi().fg(Ansi.Color.YELLOW).bold().toString() + "[RegionMarket] " + msg + Ansi.ansi().reset());
    }

    public static void severe(String msg){
        logger.severe(Ansi.ansi().fg(Ansi.Color.RED).bold().toString() + "[RegionMarket] " + msg + Ansi.ansi().reset());
    }

    public static void debug(String msg){
        logger.warning(Ansi.ansi().fg(Ansi.Color.CYAN).bold().toString() + "[DEBUG] " + msg + Ansi.ansi().reset());
    }

    public static void print(String msg){
        logger.info(msg);
    }

}
