package net.plussycraft.regionmarket.utils;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.concurrent.TimeUnit;

public class ChatUtils {

	/*
	 * Caracteres Uteis:
	 * ‣ 
	 * 
	 */
	
	
	public static void msg(CommandSender p, String msg){
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
	}
	
	public static void msg(Player p, String msg){
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
	}
	
	public static String colorize(String msg){
		msg = ChatColor.translateAlternateColorCodes('&', msg);
		return msg;
	}
	
	public static String centerString(String string){
		
		String centeredString = "";
		
		int spaces = (75 - string.length()) / 2;
		
		for(int i = 0; i < spaces; i++){
			centeredString = centeredString + " ";
		}
		
		centeredString = centeredString + string;
		
		return centeredString;
	}
	
	public static String buildString(String[] args){
		
		String msg = "";

        boolean first = true;

		for (String arg : args){
			if(first){
                msg = msg + arg;
                first = false;
            }
            else{
                msg = msg + " " + arg;
            }
		}
		
		return msg;
	}

	public static String buildString(String[] args, int ignore){

		String msg = "";

		for(int i = ignore; i < args.length; i++){
			msg = msg + args[i] + " ";
		}

		return msg;
	}
	
	// resolver a cor dos pontos
	
	public static String formatPoints(int points){
		
		String formattedPoints = "";
		
		if(points < 0) formattedPoints = ChatColor.DARK_RED + String.valueOf(points);
		if(points == 0) formattedPoints = ChatColor.GRAY + String.valueOf(points);
		if(points > 0) formattedPoints = ChatColor.GOLD + String.valueOf(points);
		
		return formattedPoints;
	}

	public static String oldConvertMillis(long millis){

		String timeFormatted;

		long seconds = TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis));
		long minutes = TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis));
		long hours = TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis));
		long days = TimeUnit.MILLISECONDS.toDays(millis);

		if(days >= 1) timeFormatted = days + " dias, " + hours + " horas, " + minutes + " minutos e " + seconds + " segundos";
		else if(hours >= 1) timeFormatted = hours + " horas, " + minutes + " minutos e " + seconds + " segundos";
		else if(minutes >= 1) timeFormatted = minutes + " minutos e " + seconds + " segundos";
		else if(seconds >= 1) timeFormatted = seconds + " segundos";
		else timeFormatted = "Menos de 1 Segundo";

		return timeFormatted;
	}

    public static String convertMillis(long millis){

        String timeFormatted = "";

        long seconds = (millis / 1000) % 60;
        long minutes = (millis / (1000 * 60)) % 60;
        long hours = (millis / (1000 * 60 * 60)) % 24;

        if (hours > 1 && minutes > 1 && seconds > 1) timeFormatted = hours + " horas, " + minutes + " minutos e " + seconds + " segundos";
        else if (hours > 1 && minutes > 1 && seconds == 1) timeFormatted = hours + " horas, " + minutes + " minutos e " + seconds + " segundo";
        else if (hours > 1 && minutes == 1 && seconds > 1) timeFormatted = hours + " horas, " + minutes + " minuto e " + seconds + " segundos";
        else if (hours > 1 && minutes == 1 && seconds == 1) timeFormatted = hours + " horas, " + minutes + " minuto e " + seconds + " segundo";
        else if (hours == 1 && minutes > 1 && seconds > 1) timeFormatted = hours + " hora, " + minutes + " minutos e " + seconds + " segundos";
        else if (hours == 1 && minutes > 1 && seconds == 1) timeFormatted = hours + " hora, " + minutes + " minutos e " + seconds + " segundo";
        else if (hours == 1 && minutes == 1 && seconds > 1) timeFormatted = hours + " hora, " + minutes + " minuto e " + seconds + " segundos";
        else if (hours == 1 && minutes == 1 && seconds == 1) timeFormatted = hours + " hora, " + minutes + " minuto e " + seconds + " segundo";
        else if (hours > 1 && minutes < 1 && seconds < 1) timeFormatted = hours + " horas";
        else if (hours > 1 && minutes > 1 && seconds < 1) timeFormatted = hours + " horas e " + minutes + " minutos";
        else if (hours == 1 && minutes < 1 && seconds < 1) timeFormatted = hours + " hora";
        else if (hours == 1 && minutes > 1 && seconds < 1) timeFormatted = hours + " hora e " + minutes + " minutos";

        else if (hours < 1 && minutes > 1 && seconds > 1) timeFormatted = minutes + " minutos e " + seconds + " segundos";
        else if (hours < 1 && minutes > 1 && seconds == 1) timeFormatted = minutes + " minutos e " + seconds + " segundo";
        else if (hours < 1 && minutes == 1 && seconds > 1) timeFormatted = minutes + " minuto e " + seconds + " segundos";
        else if (hours < 1 && minutes == 1 && seconds == 1) timeFormatted = minutes + " minuto e " + seconds + " segundo";
        else if (hours < 1 && minutes > 1 && seconds < 1) timeFormatted = minutes + " minutos";
        else if (hours < 1 && minutes == 1 && seconds < 1) timeFormatted = minutes + " minuto";

        else if (hours < 1 && minutes < 1 && seconds > 1) timeFormatted = seconds + " segundos";
        else if (hours < 1 && minutes < 1 && seconds == 1) timeFormatted = seconds + " segundo";
        else if (hours < 1 && minutes < 1 && seconds < 1) timeFormatted = "Pagamento em Curso";

        return timeFormatted;
    }

    public static String stripColor(String s){
        String r = "";
        r = ChatColor.stripColor(s);
        r = r
                .replace("§1", "")
                .replace("§2", "")
                .replace("§3", "")
                .replace("§4", "")
                .replace("§5", "")
                .replace("§6", "")
                .replace("§7", "")
                .replace("§8", "")
                .replace("§9", "")
                .replace("§0", "")
                .replace("§a", "")
                .replace("§b", "")
                .replace("§c", "")
                .replace("§d", "")
                .replace("§e", "")
                .replace("§f", "");
        return r;
    }

}
