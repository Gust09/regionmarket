package net.plussycraft.regionmarket.listeners;

import net.plussycraft.regionmarket.Main;
import net.plussycraft.regionmarket.gui.CellTakenMenu;
import net.plussycraft.regionmarket.managers.CellManager;
import net.plussycraft.regionmarket.objects.Cell;
import net.plussycraft.regionmarket.utils.ChatUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Gust09 on 05/07/2016.
 */
public class CellTakenMenuListener implements Listener {

    @EventHandler
    public void onMenuClick(InventoryClickEvent e) {

        Player p = (Player) e.getWhoClicked();

        if (e.getClickedInventory() == null)
            return;

        if (e.getClickedInventory().getName().equals(ChatUtils.colorize("&6&lCELA OCUPADA"))) {
            e.setCancelled(true);

            ItemStack item = e.getCurrentItem();

            if (!item.hasItemMeta())
                return;

            String itemName = ChatUtils.stripColor(item.getItemMeta().getDisplayName());

            if (itemName.equals("Teleportar para a Cela")) {
                Cell cell = Main.getCellByName(CellTakenMenu.MENUS.get(p.getName()).getSelected().getItemMeta().getDisplayName().split(" ")[1]);
                if (cell == null) {
                    return;
                }
                CellManager.teleport(p, cell);
            } else if (itemName.equals("Fechar")) {
                p.closeInventory();
            }

        }

    }

}
