package net.plussycraft.regionmarket.listeners;

import net.plussycraft.regionmarket.Main;
import net.plussycraft.regionmarket.gui.ConfirmUnrentMenu;
import net.plussycraft.regionmarket.managers.CellManager;
import net.plussycraft.regionmarket.objects.Cell;
import net.plussycraft.regionmarket.utils.ChatUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Gust09 on 05/07/2016.
 */
public class CPMenuListener implements Listener {

    @EventHandler
    public void onMenuClick(InventoryClickEvent e) {

        Player p = (Player) e.getWhoClicked();

        if (e.getClickedInventory() == null)
            return;

        if (e.getClickedInventory().getName().equals(ChatUtils.colorize("&6&lA TUA CELA"))) {
            e.setCancelled(true);

            ItemStack item = e.getCurrentItem();

            if (!item.hasItemMeta())
                return;

            String itemName = ChatUtils.stripColor(item.getItemMeta().getDisplayName());
            Cell cell = Main.getCellByPlayer(p.getName());

            if (cell == null) {
                return;
            }

            SignListener.setSelected(p.getName(), cell);

            if (itemName.equals("Teleportar para a Cela")) {
                CellManager.teleport(p, cell);
            } else if (itemName.equals("Abandonar a Cela")) {
                ConfirmUnrentMenu.setup(p);
            } else if (itemName.equals("Fechar")) {
                p.closeInventory();
            }

        }

    }

}
