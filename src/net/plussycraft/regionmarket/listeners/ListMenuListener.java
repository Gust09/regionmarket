package net.plussycraft.regionmarket.listeners;

import net.plussycraft.regionmarket.Main;
import net.plussycraft.regionmarket.gui.*;
import net.plussycraft.regionmarket.objects.Cell;
import net.plussycraft.regionmarket.utils.ChatUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Gust09 on 05/07/2016.
 */
public class ListMenuListener implements Listener {

    @EventHandler
    public void onMenuClick(InventoryClickEvent e) {

        Player p = (Player) e.getWhoClicked();

        if (e.getClickedInventory() == null)
            return;

        if (e.getClickedInventory().getName().equals(ChatUtils.colorize("&6&lLISTA DAS CELAS"))) {
            e.setCancelled(true);

            ItemStack item = e.getCurrentItem();

            if (item.equals(ListMenu.PAGE_BEFORE)) {
                ListMenu.MENUS.get(p.getName()).show(p, ListMenu.PAGES.get(p.getName()) - 1);
            } else if (item.equals(ListMenu.PAGE_NEXT)) {
                ListMenu.MENUS.get(p.getName()).show(p, ListMenu.PAGES.get(p.getName()) + 1);
            } else if (item.equals(ListMenu.GO_BACK)) {
                ListHubMenu.setup(p);
            } else if (item.hasItemMeta()) {
                String itemName = ChatUtils.stripColor(item.getItemMeta().getDisplayName());
                if (itemName.contains("CELA")) {
                    String id = itemName.split(" ")[1];
                    Cell cell = Main.getCellByName(id);
                    if (cell != null) {
                        SignListener.setSelected(p.getName(), cell);

                        if (cell.getOwner().isEmpty()) {
                            new EmptyCellMenu().show(p, item);
                        } else if (cell.getOwner().equalsIgnoreCase(p.getName())) {
                            new CPMenu().show(p);
                        } else if (!cell.getOwner().isEmpty()) {
                            new CellTakenMenu().show(p, item);
                        }
                    }

                }

            }

        }

    }

}
