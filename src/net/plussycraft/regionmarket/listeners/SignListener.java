package net.plussycraft.regionmarket.listeners;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import net.plussycraft.regionmarket.Main;
import net.plussycraft.regionmarket.managers.CellManager;
import net.plussycraft.regionmarket.objects.Cell;
import net.plussycraft.regionmarket.storage.CellFiles;
import net.plussycraft.regionmarket.utils.ChatUtils;
import net.plussycraft.regionmarket.utils.WGUtils;
import net.plussycraft.regionmarket.worldutils.SchematicAPI;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

/**
 * Created by Gust09 on 27/06/2016.
 */
public class SignListener implements Listener {

    private static SchematicAPI schematicAPI = new SchematicAPI(Main.get());
    private static HashMap<String, Sign> pending = new HashMap<>();
    private static HashMap<String, Cell> selected = new HashMap<>();

    @EventHandler
    public void onSignChange(SignChangeEvent e){

        if(!e.getPlayer().hasPermission("regionmarket.cell.create")){
            ChatUtils.msg(e.getPlayer(), "&6Prison &8> &4Não tens permissão!");
            return;
        }

        if(!e.getLine(0).equalsIgnoreCase("rm"))
            return;

        Player p = e.getPlayer();
        Sign s = (Sign) e.getBlock().getState();

        pending.put(p.getName(), s);
        ChatUtils.msg(p, "&eClica dentro da região para a registares");

    }

    @EventHandler
    public void onPunch(PlayerInteractEvent e){

        if(pending.containsKey(e.getPlayer().getName())){

            if(!e.getPlayer().hasPermission("regionmarket.cell.create")){
                ChatUtils.msg(e.getPlayer(), "&6Prison &8> &4Não tens permissão!");
                return;
            }

            e.setCancelled(true);
            Player p = e.getPlayer();
            Location loc = e.getClickedBlock().getLocation();
            ProtectedRegion region = WGUtils.getRegion(loc);

            if(pending.get(p.getName()).getLocation().equals(loc)){
                removePending(p.getName());
                ChatUtils.msg(p, "&6Prison &8> &aCancelaste a criação da nova cela!");
            }
            else if(region != null){

                Cell cell = new Cell(region, pending.get(p.getName()));

                String owner = "";
                for(String s : region.getOwners().getPlayerDomain().getPlayers()){
                    owner = s;
                    break;
                }

                if(!owner.isEmpty()){
                    CellManager.rent(owner, cell);
                }

                Main.addCell(cell);
                CellFiles.save(cell);
                CellManager.save(cell);
                pending.remove(p.getName());

                ChatUtils.msg(p, "&aA cela foi criada");
            }
            else{
                ChatUtils.msg(p, "&cNenhuma região encontrada");
            }
        }
        else{

            if(!e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
                return;

            if(!(e.getClickedBlock().getType().equals(Material.SIGN_POST) || e.getClickedBlock().getType().equals(Material.WALL_SIGN)))
            return;

            Player p = e.getPlayer();

            Sign sign = (Sign) e.getClickedBlock().getState();
            Cell cell = null;

            for(Cell c : Main.getCells()){
                if(c.getSign().getLocation().equals(sign.getLocation()))
                    cell = c;
            }

            if(cell == null)
                return;

            selected.put(p.getName(), cell);

            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.get(), () -> selected.remove(p.getName()), 300L);

            /*if(cell.getOwner().isEmpty()){   -----------  This shit cant be fetched from the config because of file encoding problems with intellij or windows
                for(String line : Config.getFreeList()){
                    ChatUtils.msg(p, line
                            .replace("%dimensions%", cell.getDimentions())
                            .replace("%price%", (int) cell.getPrice()+"")
                    );
                }
            }
            else if(cell.getOwner().equals(p.getName())){
                for(String line : Config.getOwnerList()){
                    ChatUtils.msg(p, line
                            .replace("%dimensions%", cell.getDimentions())
                            .replace("%price%", (int) cell.getPrice()+"")
                            .replace("%owner%", cell.getOwner())
                            .replace("%timeleft%", ChatUtils.convertMillis(cell.getLastpaid() + cell.getPaydelay() - System.currentTimeMillis()))
                    );
                }
            }
            else if(!cell.getOwner().isEmpty()){

                for(String line : Config.getTakenList()){
                    ChatUtils.msg(p, line
                            .replace("%dimensions%", cell.getDimentions())
                            .replace("%price%", (int) cell.getPrice()+"")
                            .replace("%owner%", cell.getOwner())
                    );
                }

            }*/

            String canRent = "";

            if(CellManager.getPlayersRenting().contains(p.getName())){
                canRent = "&6Não &4[Já tens uma cela]";
            }
            if(cell.getOwner().isEmpty()){
                if(canRent.isEmpty())
                    canRent = "&6Sim &a[/arrendar]";
                ChatUtils.msg(p, "&f&m-----------------+&e Informação da Cela &f&m+----------------");
                ChatUtils.msg(p, " &eDono: &6Sem Dono");
                ChatUtils.msg(p, " &eDimensões: &6" + cell.getDimentions());
                ChatUtils.msg(p, " &eRenda/Dia: &6" + (int) cell.getPrice() + "€");
                ChatUtils.msg(p, "");
                ChatUtils.msg(p, " &ePodes arrendar? " + canRent);
                ChatUtils.msg(p, "&f&m-----------------------------------------------------");
            }
            else if(cell.getOwner().equalsIgnoreCase(p.getName())){
                canRent = "&6" + ChatUtils.convertMillis(cell.getLastpaid() + cell.getPaydelay() - System.currentTimeMillis());
                ChatUtils.msg(p, "&f&m-----------------+&e Informação da Cela &f&m+----------------");
                ChatUtils.msg(p, " &eDono: &6" + cell.getOwner());
                ChatUtils.msg(p, " &eDimensões: &6" + cell.getDimentions());
                ChatUtils.msg(p, " &eRenda/Dia: &6" + (int) cell.getPrice() + "€");
                ChatUtils.msg(p, "");
                ChatUtils.msg(p, " &ePróxima Renda: " + canRent);
                ChatUtils.msg(p, "&f&m-----------------------------------------------------");
            }
            else if(!cell.getOwner().isEmpty()){
                canRent = "&6Não &4[Já tem dono]";
                ChatUtils.msg(p, "&f&m-----------------+&e Informação da Cela &f&m+----------------");
                ChatUtils.msg(p, " &eDono: &6" + cell.getOwner());
                ChatUtils.msg(p, " &eDimensões: &6" + cell.getDimentions());
                ChatUtils.msg(p, " &eRenda/Dia: &6" + (int) cell.getPrice() + "€");
                ChatUtils.msg(p, "");
                ChatUtils.msg(p, " &ePodes arrendar? " + canRent);
                ChatUtils.msg(p, "&f&m-----------------------------------------------------");
            }

        }

    }

    @EventHandler
    public void onBreak(BlockBreakEvent e){

        Player p = e.getPlayer();
        Block b = e.getBlock();

        if(b.getType().equals(Material.SIGN_POST) || b.getType().equals(Material.WALL_SIGN)){

            if(!p.hasPermission("regionmarket.cell.remove")){
                ChatUtils.msg(p, "&6Prison &8> &4Não tens permissão!");
                return;
            }

            Sign sign = (Sign) b.getState();
            Cell cell = null;

            for(Cell c : Main.getCells()){
                if(c.getSign().getLocation().equals(sign.getLocation()))
                    cell = c;
            }

            if(cell == null)
                return;

            if(!p.hasPermission("pcm.remove"))
                return;

            CellFiles.delete(cell.getName());
            schematicAPI.delete(cell.getName());
            Main.removeCell(cell);

            ChatUtils.msg(p, "&cA cela foi removida");


        }

    }

    public static Cell getSelected(String name){
        Cell cell = null;
        if(selected.containsKey(name))
            cell = selected.get(name);
        return cell;
    }

    public static void setSelected(String name, Cell cell){
        selected.put(name, cell);
    }

    public static boolean isPending(String name){
        return pending.containsKey(name);
    }

    public static void removePending(String name){
        Sign sign = pending.get(name);
        sign.getBlock().setType(Material.AIR);
        sign.getWorld().dropItem(sign.getLocation(), new ItemStack(Material.SIGN));
        pending.remove(name);
    }

}