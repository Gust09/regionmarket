package net.plussycraft.regionmarket.listeners;

import net.plussycraft.regionmarket.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkUnloadEvent;

/**
 * Created by Gust09 on 07/07/2016.
 */
public class ChunkListener implements Listener {

    @EventHandler
    public void onChunkUnload(ChunkUnloadEvent e) {

        if (Main.getChunks().contains(e.getChunk())) {
            e.setCancelled(true);
        }


    }


}
