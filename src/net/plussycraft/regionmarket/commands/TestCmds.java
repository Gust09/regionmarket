package net.plussycraft.regionmarket.commands;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import net.plussycraft.regionmarket.Main;
import net.plussycraft.regionmarket.objects.Cell;
import net.plussycraft.regionmarket.utils.ChatUtils;
import net.plussycraft.regionmarket.utils.WGUtils;
import net.plussycraft.regionmarket.worldutils.SchematicAPI;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Gust09 on 27/06/2016.
 */
public class TestCmds implements CommandExecutor {

    SchematicAPI schematicAPI = new SchematicAPI(Main.get());
    private static Block pos1;
    private static Block pos2;

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String l, String[] args) {

        if(!(sender instanceof Player))
            return false;

        Player p = (Player) sender;

        if(l.equalsIgnoreCase("save")){

            ProtectedRegion region = WGUtils.getRegion(p.getWorld(), args[0]);

            pos1 = new Location(p.getWorld(), (double) region.getMinimumPoint().getBlockX(), (double) region.getMinimumPoint().getBlockY(), (double) region.getMinimumPoint().getBlockZ()).getBlock();
            pos2 = new Location(p.getWorld(), (double) region.getMaximumPoint().getBlockX(), (double) region.getMaximumPoint().getBlockY(), (double) region.getMaximumPoint().getBlockZ()).getBlock();

            if(pos1 == null){
                ChatUtils.msg(p, "Pos. 1 not found");
                return false;
            }

            if(pos2 == null){
                ChatUtils.msg(p, "Pos. 2 not found");
                return false;
            }

            ChatUtils.msg(p, "Saving...");
            schematicAPI.save(args[0], schematicAPI.getStructure(pos1, pos2));
            ChatUtils.msg(p, "Schem saved");
        }

        else if(l.equalsIgnoreCase("paste")){

            ProtectedRegion region = WGUtils.getRegion(p.getWorld(), args[0]);
            Location loc = new Location(p.getWorld(), (double) region.getMinimumPoint().getBlockX(), (double) region.getMinimumPoint().getBlockY(), (double) region.getMinimumPoint().getBlockZ());

            ChatUtils.msg(p, "Pasting...");

            schematicAPI.paste(schematicAPI.load(args[0]), loc);
            ChatUtils.msg(p, "Done!");
        }

        else if(l.equalsIgnoreCase("test")){
            for(Cell c : Main.getCells()){

                ChatUtils.msg(p, "Name: " + c.getRegion().getId());
                ChatUtils.msg(p, "Dimensions: " + c.getX()+"x"+c.getY()+"x"+c.getZ());
                ChatUtils.msg(p, "Area: " + c.getArea());
                ChatUtils.msg(p, "Price: " + c.getPrice());


            }
        }


        return false;
    }
}
