package net.plussycraft.regionmarket.commands;

import net.plussycraft.regionmarket.Main;
import net.plussycraft.regionmarket.listeners.SignListener;
import net.plussycraft.regionmarket.managers.CellManager;
import net.plussycraft.regionmarket.objects.Cell;
import net.plussycraft.regionmarket.utils.ChatUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Set;

/**
 * Created by Gust09 on 27/06/2016.
 */
public class AdminCmd implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String l, String[] args) {

        if(!(sender instanceof Player))
            return false;

        Player p = (Player) sender;

        if(args.length > 0){

            if(args[0].equalsIgnoreCase("rent")){

                if(args.length != 2){
                    ChatUtils.msg(p, "&6Prison &8> &4Uso Correto: /rma rent <jogador>");
                    return false;
                }

                String name = args[1];

                Location loc = getTargetLocation(p);
                Cell cell = null;

                for(Cell c : Main.getCells()){
                    if(c.getSign().getLocation().equals(loc)){
                        cell = c;
                        break;
                    }
                }

                if(cell == null){
                    ChatUtils.msg(p, "&6Prison &8> &4Não estás a olhar para nenhuma placa de Cela");
                    return false;
                }

                if(!cell.getOwner().isEmpty()){
                    CellManager.unrent(cell);
                }

                if(CellManager.rent(name, cell)){
                    ChatUtils.msg(p, "&6Prison &8> &aArrendaste a cela a " + name);
                }
                else{
                    ChatUtils.msg(p, "&6Prison &8> &4Ocorreu um erro ao arrendar a cela a " + name);
                }

            }
            else if(args[0].equalsIgnoreCase("unrent")){

                Location loc = getTargetLocation(p);
                Cell cell = null;

                for(Cell c : Main.getCells()){
                    if(c.getSign().getLocation().equals(loc)){
                        cell = c;
                        break;
                    }
                }

                if(cell == null){
                    ChatUtils.msg(p, "&6Prison &8> &4Não estás a olhar para nenhuma placa de Cela");
                    return false;
                }

                if(!cell.getOwner().isEmpty()){
                    CellManager.unrent(cell);
                    ChatUtils.msg(p, "&6Prison &8> &aA cela está agora livre");
                }
                else{
                    ChatUtils.msg(p, "&6Prison &8> &4Ocorreu um erro a resetar a cela");
                }


            }
            else if(args[0].equalsIgnoreCase("cancel")){
                if(!p.hasPermission("regionmarket.cell.create")){
                    ChatUtils.msg(p, "&6Prison &8> &4Não tens permissão!");
                    return false;
                }
                if(SignListener.isPending(p.getName())){
                    SignListener.removePending(p.getName());
                    ChatUtils.msg(p, "&6Prison &8> &aCancelaste a criação da nova cela!");
                }
                else{
                    ChatUtils.msg(p, "&6Prison &8> &4Não estás a criar uma cela!");
                }
            }
        }
        else{
            // TODO info here?
        }



        return false;
    }

    private Location getTargetLocation(Player p){
        Location l = p.getTargetBlock((Set<Material>) null, 15).getLocation();
        return l;
    }

}
