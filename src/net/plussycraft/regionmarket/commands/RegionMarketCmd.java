package net.plussycraft.regionmarket.commands;

import net.milkbowl.vault.economy.EconomyResponse;
import net.plussycraft.regionmarket.Main;
import net.plussycraft.regionmarket.gui.CPMenu;
import net.plussycraft.regionmarket.gui.MainMenu;
import net.plussycraft.regionmarket.listeners.SignListener;
import net.plussycraft.regionmarket.managers.CellManager;
import net.plussycraft.regionmarket.objects.Cell;
import net.plussycraft.regionmarket.utils.ChatUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Gust09 on 27/06/2016.
 */
public class RegionMarketCmd implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String l, String[] args) {

        if(!(sender instanceof Player))
            return false;

        Player p = (Player) sender;

        if(args.length > 0){

            if(args[0].equalsIgnoreCase("rent")){

                if(!p.hasPermission("regionmarket.cmd.rent")){
                    ChatUtils.msg(p, "&6Prison &8> &4Não tens permissão!");
                    return false;
                }

                Cell cell = SignListener.getSelected(p.getName());

                if(cell != null){
                    if(CellManager.getPlayersRenting().contains(p.getName())){
                        ChatUtils.msg(p, "&6Prison &8> &4Tu já tens uma cela arrendada!");
                    }
                    else if(cell.getOwner().isEmpty()){

                        EconomyResponse r = Main.econ.withdrawPlayer(p.getName(), cell.getPrice());

                        if(r.transactionSuccess()){
                            if(CellManager.rent(p.getName(), cell)){
                                ChatUtils.msg(p, "&6Prison &8> &aTu pagaste &e"+(int)cell.getPrice()+"€ &apara arrendar esta cela e és agora o dono dela. Utiliza /cela ou /home para veres as opções da tua cela!");
                            }
                            else{
                                ChatUtils.msg(p, "&4Ocorreu um erro... Tenta novamente mais tarde.");
                            }
                        }
                        else{
                            ChatUtils.msg(p, "&6Prison &8> &4Não tens dinheiro suficiente para arrendar essa cela!");
                        }
                    }
                    else if(cell.getOwner().equalsIgnoreCase(p.getName())){
                        ChatUtils.msg(p, "&6Prison &8> &4Esta cela já é tua!");
                    }
                    else if(!cell.getOwner().isEmpty()){
                        ChatUtils.msg(p, "&6Prison &8> &4Esta cela já tem dono!");
                    }
                }
                else{
                    ChatUtils.msg(p, "&6Prison &8> &4Tu não tens nenhuma cela selecionada! Seleciona uma clicando na placa que está na parede da cela!");
                }

            }
            else if(args[0].equalsIgnoreCase("unrent")){

                if(!p.hasPermission("regionmarket.cmd.unrent")){
                    ChatUtils.msg(p, "&6Prison &8> &4Não tens permissão!");
                    return false;
                }

                Cell cell = SignListener.getSelected(p.getName());

                if(cell != null){
                    if(cell.getOwner().equalsIgnoreCase(p.getName())){
                        if(CellManager.unrent(cell)){
                            ChatUtils.msg(p, "&6Prison &8> &aTu abandonaste a tua cela!");
                        }
                        else{
                            ChatUtils.msg(p, "&4Ocorreu um erro. Tenta novamente mais tarde.");
                        }
                    }
                    else if(!cell.getOwner().equalsIgnoreCase(p.getName())){
                        ChatUtils.msg(p, "&6Prison &8> &4Tu apenas podes anular a renda da tua própria cela!");
                    }
                    else if(cell.getOwner().isEmpty()){
                        ChatUtils.msg(p, "&6Prison &8> &4Tu apenas podes anular a renda da tua própria cela!");
                    }
                }
                else{
                    ChatUtils.msg(p, "&6Prison &8> &4Tu não tens nenhuma cela selecionada! Seleciona uma clicando na placa que está na parede da cela!");
                }

            }
            else if(args[0].equalsIgnoreCase("tp")){

                Cell cell = Main.getCellByPlayer(p.getName());

                if (cell == null) {
                    ChatUtils.msg(p, "&6Prison &8> &4Não tens uma cela!");
                    return false;
                }

                CellManager.teleport(p, cell);

            }
            else if(args[0].equalsIgnoreCase("list")){

                if(!p.hasPermission("regionmarket.cmd.list")){
                    ChatUtils.msg(p, "&6Prison &8> &4Não tens permissão!");
                    return false;
                }

                //new ListMenu().show(p, 1);
            } else if (args[0].equalsIgnoreCase("blabla-mainmenu")) {

                if (!p.hasPermission("regionmarket.cmd.mainmenu")) {
                    ChatUtils.msg(p, "&6Prison &8> &4Não tens permissão!");
                    return false;
                }
                MainMenu.setup(p);
            }

            else if (args[0].equalsIgnoreCase("controlpanel")) {
                Cell cell = Main.getCellByPlayer(p.getName());
                if (cell == null) {
                    ChatUtils.msg(p, "&6Prison &8> &4Tu não tens nenhuma cela arrendada!");
                    return false;
                }
                new CPMenu().show(p);
            }

        }
        else{
            // TODO info here?
        }



        return false;
    }
}
