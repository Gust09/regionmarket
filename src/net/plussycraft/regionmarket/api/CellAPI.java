package net.plussycraft.regionmarket.api;

import net.plussycraft.regionmarket.Main;
import net.plussycraft.regionmarket.listeners.SignListener;
import net.plussycraft.regionmarket.managers.CellManager;
import net.plussycraft.regionmarket.objects.Cell;

/**
 * Created by Gust09 on 04/07/2016.
 */
public class CellAPI {

    public CellAPI(){}

    public boolean isRenting(String name){
        return CellManager.getPlayersRenting().contains(name);
    }

    public boolean forceUnrent(String name){
        Cell cell = null;

        for(Cell c : Main.getCells()){
            if(c.getOwner().equalsIgnoreCase(name)) {
                cell = c;
                break;
            }
        }

        if(cell != null){
            CellManager.unrent(cell);
            return true;
        }
        else{
            return false;
        }
    }

    public boolean isOwnCellSelected(String name){
        Cell cell = SignListener.getSelected(name);
        if(cell.getOwner().equalsIgnoreCase(name)){
            return true;
        }
        return false;
    }

    public boolean hasCellSelected(String name){
        return SignListener.getSelected(name) != null;
    }


}
