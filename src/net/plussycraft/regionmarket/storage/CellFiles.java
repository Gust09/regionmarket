package net.plussycraft.regionmarket.storage;

import net.plussycraft.regionmarket.Main;
import net.plussycraft.regionmarket.objects.Cell;

import java.io.*;

/**
 * Created by Gust09 on 30/06/2016.
 */
public class CellFiles {


    public static void save(Cell cell){
        ObjectOutputStream oos = null;
        FileOutputStream fout = null;

        File f = new File(Main.get().getDataFolder() + "/cells/" + cell.getRegion().getId() + ".cell");
        File dir = new File(Main.get().getDataFolder() + "/cells");

        try {
            dir.mkdirs();
            f.createNewFile();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        try{
            fout = new FileOutputStream(f);
            oos = new ObjectOutputStream(fout);
            oos.writeObject(cell);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(oos  != null){
                try {
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void load(String name){

        File f = new File(Main.get().getDataFolder() + "/cells/" + name + ".cell");

        try {
            FileInputStream streamIn = new FileInputStream(f);
            ObjectInputStream objectinputstream = new ObjectInputStream(streamIn);
            Cell cell = (Cell)objectinputstream.readObject();
            cell.load();
            Main.addCell(cell);

            objectinputstream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void delete(String name){
        File f = new File(Main.get().getDataFolder() + "/cells/" + name + ".cell");
        try {
            f.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
