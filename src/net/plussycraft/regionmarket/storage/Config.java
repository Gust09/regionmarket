package net.plussycraft.regionmarket.storage;

import net.plussycraft.regionmarket.Main;

import java.util.List;

/**
 * Created by Gust09 on 30/06/2016.
 */
public class Config {

    private static Main pl = Main.get();

    public static void setup(){
        pl.getConfig().options().copyDefaults(true);
        pl.saveConfig();
    }

    public static long getRenewDelay(){
        //return pl.getConfig().getLong("config.rent-renew-delay-in-millis");
        return 86400000L;
    }

    public static List<String> getFreeList(){
        return pl.getConfig().getStringList("lang.sign-click-free");
    }

    public static List<String> getTakenList(){
        return pl.getConfig().getStringList("lang.sign-click-taken");
    }

    public static List<String> getOwnerList(){
        return pl.getConfig().getStringList("lang.sign-click-owner");
    }

}
