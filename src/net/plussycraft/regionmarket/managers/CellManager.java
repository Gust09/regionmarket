package net.plussycraft.regionmarket.managers;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.managers.storage.StorageException;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import net.milkbowl.vault.economy.EconomyResponse;
import net.plussycraft.regionmarket.Main;
import net.plussycraft.regionmarket.objects.Cell;
import net.plussycraft.regionmarket.storage.CellFiles;
import net.plussycraft.regionmarket.utils.ChatUtils;
import net.plussycraft.regionmarket.utils.WGUtils;
import net.plussycraft.regionmarket.worldutils.SchematicAPI;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.material.Sign;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gust09 on 01/07/2016.
 */
public class CellManager {

    private static SchematicAPI schematicAPI = new SchematicAPI(Main.get());

    private static List<String> playersRenting = new ArrayList<>();

    public static void runCheckTimer(){
        Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.get(), () -> {

            long current = System.currentTimeMillis();

            for(Cell c : Main.getCells()){

                if(c.getOwner().isEmpty())
                    continue;

                if((c.getLastpaid() + c.getPaydelay() - current) < 0){

                    EconomyResponse r = Main.econ.withdrawPlayer(c.getOwner(), c.getPrice());
                    if(r.transactionSuccess()){
                        c.setLastpaid(current);
                        OfflinePlayer op = Bukkit.getOfflinePlayer(c.getOwner());
                        if(op.isOnline()){
                            Player p = (Player) op;
                            ChatUtils.msg(p, "&6Prison &8> &aForam removidos &e"+(int)c.getPrice()+"€ &ada tua conta para poderes pagar a renda da tua cela!");
                        }
                    }
                    else{
                        unrent(c);

                        OfflinePlayer op = Bukkit.getOfflinePlayer(c.getOwner());
                        if(op.isOnline()){
                            Player p = (Player) op;
                            ChatUtils.msg(p, "&6Prison &8> &4Tu perdeste a tua cela porque não tinhas dinheiro suficiente para pagar a renda!");
                        }


                    }

                }

            }


        }, 200L, 60L*20L);
    }

    public static boolean rent(String name, Cell cell) {
        boolean r = false;

        if (cell.getOwner().isEmpty()) {

            cell.setOwner(name);

            try {
                DefaultDomain dd = cell.getRegion().getOwners();
                dd.addPlayer(name);
                cell.getRegion().setOwners(dd);
                WGBukkit.getRegionManager(cell.getWorld()).save();
            } catch (StorageException e) {
                e.printStackTrace();
            }

            cell.setLastpaid(System.currentTimeMillis());
            cell.updateSign();
            CellFiles.save(cell);
            playersRenting.add(cell.getOwner());
            r = true;

        } else {
            r = false;
        }

        return r;
    }

    public static boolean unrent(Cell cell) {
        boolean r = false;

        if (!cell.getOwner().isEmpty()) {

            playersRenting.remove(cell.getOwner());
            cell.setOwner("");

            try {
                DefaultDomain dd = cell.getRegion().getOwners();
                dd.removeAll();
                cell.getRegion().setOwners(dd);
                WGBukkit.getRegionManager(cell.getWorld()).save();
            } catch (StorageException e) {
                e.printStackTrace();
            }

            cell.setLastpaid(0L);
            cell.updateSign();
            CellFiles.save(cell);
            restore(cell);
            r = true;

        } else {
            r = false;
        }

        return r;
    }

    public static boolean save(Cell cell){
        ProtectedRegion region = WGUtils.getRegion(cell.getSign().getWorld(), cell.getName());

        Block pos1, pos2;

        pos1 = new Location(cell.getSign().getWorld(), (double) region.getMinimumPoint().getBlockX(), (double) region.getMinimumPoint().getBlockY(), (double) region.getMinimumPoint().getBlockZ()).getBlock();
        pos2 = new Location(cell.getSign().getWorld(), (double) region.getMaximumPoint().getBlockX(), (double) region.getMaximumPoint().getBlockY(), (double) region.getMaximumPoint().getBlockZ()).getBlock();

        if(pos1 == null || pos2 == null){
            return false;
        }

        schematicAPI.save(cell.getName(), schematicAPI.getStructure(pos1, pos2));
        return true;
    }


    public static void restore(Cell cell){
        ProtectedRegion region = WGUtils.getRegion(cell.getSign().getWorld(), cell.getName());
        Location loc = new Location(cell.getSign().getWorld(), (double) region.getMinimumPoint().getBlockX(), (double) region.getMinimumPoint().getBlockY(), (double) region.getMinimumPoint().getBlockZ());

        schematicAPI.paste(schematicAPI.load(cell.getName()), loc);
    }

    public static List<String> getPlayersRenting(){
        return playersRenting;
    }

    public static void updatePlayersRenting(){
        for(Cell c : Main.getCells()){
            playersRenting.add(c.getOwner());
        }
    }

    public static void teleport(Player p, Cell c) {
        Sign s = (Sign) c.getSign().getData();
        Location loc;

        switch (s.getAttachedFace()) {
            case NORTH:
                loc = c.getSign().getLocation().clone().add(0.5, -2, 0.5);
                loc.setYaw(-180);
                break;
            case EAST:
                loc = c.getSign().getLocation().clone().add(0.5, -2, 0.5);
                loc.setYaw(-90);
                break;
            case SOUTH:
                loc = c.getSign().getLocation().clone().add(0.5, -2, -0.5);
                loc.setYaw(0);
                break;
            case WEST:
                loc = c.getSign().getLocation().clone().add(-0.5, -2, 0.5);
                loc.setYaw(90);
                break;
            default:
                loc = c.getSign().getLocation().clone().add(0.5, -2, 0.5);
                break;
        }
        p.teleport(loc);

    }

}
